import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;

public class Tester {

	//public static Scoreboard sb;

	public static void main(String[] args) {
	
		//sb = new Scoreboard();
		//sb.printRegs();
		
		// register test
		Registers registers = new Registers(false);
		registers.test();
		
		// instruction cache test
		// memory cache test
		System.out.println("\n------------- INST & MEMO CACHE TEST ----------");
		Parser test = new Parser();
		ArrayList<Instruction> instCache = new ArrayList<Instruction>();
		HashMap<Integer, Double> memoCache = new HashMap<Integer, Double>();
		try {
			test.read(new FileInputStream("benchmark1.dat"));
			instCache = test.returnInstCache();
			memoCache = test.returnMemoryCache();
			
			System.out.println("******** Instruction cache **********");
			// display the items in each cache
			for(Instruction i:instCache){
				System.out.println(i.getContent());
			}
			System.out.println("******** Memory map **********\n"+memoCache.toString());
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		// circular array test
		System.out.println("\n------------- CIRCULAR ARRAY TEST ----------");
		ArrayQueue<Integer> testRS = new ArrayQueue<Integer>(5);
		for(int i=0; i<5; i++) {
			testRS.offer(i);
		}
		System.out.println(testRS.toString());
		
		// register value test
		System.out.println("\n------------- EXECUTION RESULT TEST ----------");
		Instruction inst = new Instruction();
		inst.setOperation("ORI");
		inst.setrDest("R1");
		inst.setrSrc1("R1");
		inst.setImmediate(-8);
		inst.setOffset(0);
		inst.setrSrc2("R2");
		inst.setFuncUnit("XSU");
		FuncUnit funcTest = new FuncUnit();
		System.out.println(funcTest.executeXSU(inst));
		
		// RSarray test
		System.out.println("\n------------- RS array TEST ----------");
		RsArray testArray = new RsArray(5);
		testArray.add(inst);
		System.out.println(testArray.hasInst());
		
		// ROB test
		System.out.println("\n------------- ROB TEST ----------");
		ReorderBuffer rob = new ReorderBuffer(16);
		rob.add(inst);
		System.out.println(rob.getSingleEntry("ROB0").getwbReg());
	}

}