import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ReorderBuffer {

	private int size;
	private RBEntry[] a;
	
	//circular array - maintain start and other info
	private int first;
	private int nextAvailable;
	private int last;
	private int num;

	public ReorderBuffer(int s) {
		// set size and initialize array
		size = s;
		a = new RBEntry[size];

		// initialize all variables to 0		
		first = 0;
		nextAvailable = 0;
		last = 0;
		num = 0;

	}

	// CALLS FOR STATISTICS
	public int getNumInt() {
		int count = 0;
		for (int i = 0; i < size; i++) {
			RBEntry entry = a[i];
			if (entry != null) {
				if (!entry.getFP()) {
					count++;
				}
			}
		}
		return count;
	}
	public int getNumFP(){
		int count = 0;
		for (int i = 0; i < size; i++) {
			RBEntry entry = a[i];
			if (entry != null) {
				if (entry.getFP()) {
					count++;
				}
			}
		}
		return count;
	}

	// debugging
	public void printStats() {
		System.out.println("**** stats *****");
		System.out.println("first: "+first);
		System.out.println("last: "+last);
		System.out.println("nextAvailable: "+nextAvailable);
		System.out.println("num entries: "+num);


	}



	// returns true value if rob has space, false if not
	public boolean hasSpace() {
		if ((size-num) > 0) return true;
		return false;

	}

	// returns number of spaces available 
	public int spaceAvailable() {
		return (size-num);

	}

	// return current number in ROB
	public int getNum() {
		return num;
	}

	// clear with no parameter resets all
	public void clear() {
		// initialize array
		a = new RBEntry[size];

		// initialize all variables to 0		
		first = 0;
		nextAvailable = 0;
		last = 0;
		num = 0;
	}

	// clear with parameter "ROB#"
	// SEND IN "ROB#" of BRANCH INSTRUCTION
	// clear index +1 to end
	public ArrayList<String> clear(String currentROB) { 
		ArrayList<String> arr = new ArrayList<String>();
		Pattern memoLoc = Pattern.compile("\\d+");
		Matcher match_memo = memoLoc.matcher(currentROB);
		if(match_memo.find()){
			int index = Integer.parseInt(match_memo.group(0));
			
			// get array of strings to return
			// if branch index is the last, you have nothing to clear, so return null
			if (index == last) {
				arr.add("");
				return arr;		
			}

			boolean notNextAvail = true;
			int currentInd = index+1;
			while (notNextAvail) {
				arr.add(new String("ROB"+currentInd));
				currentInd = getNextIndex(currentInd);
				if (currentInd == nextAvailable) {
					notNextAvail = false;

				}

			}	
			
			// do the actual remove here
			clear(index+1);

			//return arraylist
			if (arr.size() == 0) arr.add("");
			return arr;
			
	
		}
		arr.add("");
		return arr;
	}

	// clear with parameter - index of first invalid instruction. clears all from this ind to last
	public void clear(int index) {
		if (index == first) {
			clear();
		}
		else {
			int currentIndex = index;
			boolean notNextAvail = true;
			while (notNextAvail) {
				a[currentIndex] = null;
				num--;
				currentIndex = getNextIndex(currentIndex);
				if (currentIndex == nextAvailable) { 
					notNextAvail = false;		
				}
			}
			nextAvailable = index;
			last = getPreviousIndex(index);
		}
		return;

	}

	// clear with parameter - first invalid instruction. clears all from this ind to last
	public int clear(Instruction ins) {
		if (ins.equals(a[first].inst)) {
			clear();
		}

		for (int i = 0; i < size; i++) {
			RBEntry entry = a[i];
			if (entry != null && entry.inst.equals(ins)) {
				clear(i);
				return i;
			}

		}
		return -1;

	}

	// add an instruction to end of array - this class decides where to put it
	// returns string "ROB#"
	public String add(Instruction n) {
		String robNum = null;

		if (num < size) {
			RBEntry entry = new RBEntry(n);
			a[nextAvailable] = entry;
			robNum = "ROB"+nextAvailable;
			
			// update variables
			increaseNextAvailable();
			increaseNum();
			if (num > 1) increaseLast();

			// if this test fails, something went wrong
			if (!a[getPreviousIndex(nextAvailable)].equals(entry)) {

				robNum = null;
			}
		}

		return robNum;
	}

	// removes the "first" item in array (from index first), updates first
	public Instruction remove() {	
		
		if (num > 1) {
			RBEntry entry = a[first];

			// if entry is null, something went wrong
			if (entry == null) return null;


			// set this index to null now
			a[first] = null;

			// update variables
			increaseFirst();
			decreaseNum();

			return entry.inst;
		}
		else if (num == 1) {
			RBEntry entry = a[first];
			clear();
			if (entry == null) return null;
			return entry.inst;
		}
		return null;
	}


	// private function to increase nextAvailable
	private int increaseNextAvailable() {
		int i = nextAvailable;
		nextAvailable = getNextIndex(i);
		return nextAvailable;

	}

	// private function to decrease nextAvailable
	private int decreaseNextAvailable() {
		int i = nextAvailable;
		nextAvailable = getPreviousIndex(i);
		return nextAvailable;
	}

	// private function to increase count of total number of entries
	private int increaseNum() {
		num = num + 1;
		return num;

	}
	// private function to decrease count of total number of entries	
	private int decreaseNum() {
		if (num > 0) {
			num = num - 1;
		}
		return num;
	}

	// private function to increase index of first (earliest issued) instruction/entry
	private int increaseFirst() {
		int i = first;
		first = getNextIndex(i);
		return first;		
	}

	// private function to decrease index of first (earliest issued) instruction/entry
	private int decreaseFirst() {
		int i = first;
		first = getPreviousIndex(i);
		return first;
	}

	// private function to decrease index of last (latest issued) instruction/entry
	private int increaseLast() {
		int i = last;
		last = getNextIndex(i);
		return last;	
	}
	
	// private function to decrease index of last (latest issued) instruction/entry
	private int decreaseLast() {
		int i = last;
		last = getPreviousIndex(i);
		return last;	
	}

	// gets next increasing index (circular) 
	private int getNextIndex(int currentIndex) {
		if (currentIndex == (size-1)) {
			return 0;
		}

		return (currentIndex+1);
	}	

	
	// gets next decreasing index (circular)
	private int getPreviousIndex(int currentIndex) {	
		if (currentIndex == 0) {
			return (size-1);
		}
		return (currentIndex-1);
	}

	// get RBEntry from first, last
	public RBEntry getEntry(String pos) {
		RBEntry entry = null;		
		if (pos.equals("first")) {
			entry = getEntry(first);
		}
		else if (pos.equals("last")) {
			entry = getEntry(last);

		}
		return entry;
	}

	// get RBEntry from specific index
	public RBEntry getEntry(int index) {
		return a[index];
	}


	// prints everything in RBEntry array by order (first to last)
	public void printAllContents() {
		int i = first;	
		//System.out.println("\n**** PRINTING CONTENTS OF REORDER FIRST TO LAST (CIRCULAR) ****");	
		for(int k = 0; k < size; k++) {
			RBEntry entry = a[i];
			if (entry != null) {
				System.out.println("index: "+i);
				System.out.println("Instr: "+entry.inst.getContent());
				System.out.println("wbReg: "+entry.wbReg);
				System.out.println("fp: "+entry.fp);
				System.out.println("iVal: "+entry.iVal);
				System.out.println("fpVal: "+entry.fpVal);
				System.out.println("wbFlag: "+entry.wbFlag);
				System.out.println("");		
					
			}
			/*
			else {
				System.out.println("index: "+i);
				System.out.println("EMPTY");
			}
			*/			
			i = getNextIndex(i);	
		}

	}

	// get a single entry specified by strings "first", "last", or "ROB#"
	public RBEntry getSingleEntry(String s) {
		if (s.equals("first")) {
			return getSingleEntry(first);
		}
		else if (s.equals("last")) {
			return getSingleEntry(last);
		}
		
		// assumes string comes in "ROB#"
		Pattern memoLoc = Pattern.compile("\\d+");
		Matcher match_memo = memoLoc.matcher(s);
		if(match_memo.find()){
			int index = Integer.parseInt(match_memo.group(0));
			return getSingleEntry(index);
		}
		return null;
	}

	// set/reset a single entry based on "ROB#"
	public void setSingleEntry(String s, RBEntry entry) {
		Pattern memoLoc = Pattern.compile("\\d+");
		Matcher match_memo = memoLoc.matcher(s);
		if(match_memo.find()){
			int index = Integer.parseInt(match_memo.group(0));
			a[index] = entry;
		}
	}
	
	// get a single entry specified by index
	public RBEntry getSingleEntry(int index) {
		return a[index];
	}


	/**************** FIND & REPLACE FOR ALL ******************/
	// returns instruction with all replaceable registers replaced (for reservation station)
	// stores instruction in entry with SRC registers replaced
	public Instruction replaceAllRegs(String currentROB) {
		Instruction in = null;

		if (currentROB == null ) return null;

		Pattern memoLoc = Pattern.compile("\\d+");
		Matcher match_memo = memoLoc.matcher(currentROB);
		if(match_memo.find()){
			int origIndex = Integer.parseInt(match_memo.group(0));
			int curIndex = origIndex;

			// get instruction to replace			
			Instruction currentInstr = a[origIndex].getInstruction();	
			String dest = null;
			String sr1 = null;
			String sr2 = null;				
			
			// get functional unit to decode
			String fu = currentInstr.getFuncUnit();
			if (fu.equals("XSU")) {
				dest = currentInstr.getrDest();
				sr1 = currentInstr.getrSrc1();
				// immedates will not have sr2 - test for null
				sr2 = currentInstr.getrSrc2();
			}
			else if (fu.equals("LS") && (currentInstr.getOperation().contains("L") || currentInstr.getOperation().contains("l"))) {	
				// LW dest, offset(sr1)
				dest = currentInstr.getrDest();
				sr1 = currentInstr.getrSrc1();
				// sr2 is null
			}
			else if (fu.equals("LS") && (currentInstr.getOperation().contains("S") || currentInstr.getOperation().contains("s"))) {
				// SW sr1, offset(sr2)
				// dest is null
				sr1 = currentInstr.getrSrc1(); 
				sr2 = currentInstr.getrSrc2();
			}
			else if (fu.equals("FPU")) {
				dest = currentInstr.getrDest();
				sr1 = currentInstr.getrSrc1();
				sr2 = currentInstr.getrSrc2();
			}
			else if (fu.equals("MCF")) {
				dest = currentInstr.getrDest();
				sr1 = currentInstr.getrSrc1();
				sr2 = currentInstr.getrSrc2();
			}
			else if (fu.equals("BPU")) { // THIS ISN'T DONE OR CORRECT YET
				dest = currentInstr.getrDest();
				sr1 = currentInstr.getrSrc1();
				sr2 = currentInstr.getrSrc2();
			}
			else { dest = null; /*all remain null*/ }

			// if all are null, you can't return an instruction
			if (dest == null && sr1 == null && sr2 == null) {
				return null;
			}

			// rename sources. look for wbReg for every increasing index
			boolean reachedFirst = false;
			boolean rename1 = false;
			boolean rename2 = false;
			if (num > 1) {
				while (!reachedFirst) {
					curIndex = getPreviousIndex(curIndex);
					RBEntry curEntry = a[curIndex];
					//System.out.println(currentROB);
					//System.out.println("r.b. index: "+curIndex);
					//curEntry.printEntry();
					String wb = curEntry.getwbReg();
					///System.out.println(wb+", "+curEntry.inst.getOperation());
					///printStats();
					///System.out.println("Current index: "+curIndex);
					if (wb != null && !wb.equals("EMPTY")) {
						if (!rename1 && (sr1 != null)) {
							if (sr1.equals(curEntry.wbReg)) {
								currentInstr.setrSrc1("ROB"+curIndex);
								rename1 = true;
							}
						}
						if (!rename2 && (sr2 != null)) {
							if (sr2.equals(curEntry.wbReg)) {
								currentInstr.setrSrc2("ROB"+curIndex);
								rename2 = true;
							}
						}
					}
					if (curIndex == first || (rename1 && rename2)) {
						reachedFirst = true;
					}
				}
			}

			
			// replace with actual value (saved as string) if applicable
			if (!rename1 && sr1 != null) {
				if (sr1.contains("F") || sr1.contains("f")) {
					currentInstr.setrSrc1(String.valueOf(RunSimulation.registers.readFPReg(sr1)));
				}
				else if (sr1.contains("R") || sr1.contains("r")) {
					currentInstr.setrSrc1(String.valueOf(RunSimulation.registers.readIntReg(sr1)));
				}

			}
			if (!rename2 && sr2 != null) {
				if (sr2.contains("F") || sr2.contains("f")) {
					currentInstr.setrSrc2(String.valueOf(RunSimulation.registers.readFPReg(sr2)));
				}
				else if (sr2.contains("R") || sr2.contains("r")) {
					currentInstr.setrSrc2(String.valueOf(RunSimulation.registers.readIntReg(sr2)));
				}

			}
						

			// save this instruction to entry
			a[origIndex].resetInstr(currentInstr);
			//System.out.println("SAVED THIS INSTR IN ENTRY:");
			//System.out.println(currentInstr.getrDest()+" "+currentInstr.getrSrc1()+" "+currentInstr.getrSrc2());
			
			// rename destination with ROB#
			if (currentInstr.getrDest() != null) {
				currentInstr.setrDest(currentROB);
		
			}

			//return this instruction
			in = currentInstr;

		}		

		return in;
	}

	/**************** WRITE BACK, INCLUDES ALGORITHM TO REPLACE ROB# WITH VALUE **************/
	// tests to see if first item is commitable (contains value)
	public boolean canCommit() {
		RBEntry entry = a[first];
		if (entry != null) {
			if (entry.wbFlag) {
				return true;
			}
		}
		return false;

	}	

	// writes back single value from first ROB
	public boolean commit() {
		RBEntry entry = a[first];		
		if (entry != null) {

			// CREATE STRING FROM INDEX
			String robToReplace = "ROB"+first;
			String val = null;

			// if store word, get data from store buffer to commit to memory
			Instruction i = entry.inst;
			if (i != null && (i.getOperation().equals("SD") || i.getOperation().equals("S.D")) ) {
				if (RunSimulation.linkToSDEntry.containsKey("ROB"+first)) {				
					StoreBufferEntry sb = RunSimulation.linkToSDEntry.get("ROB"+first);
					if (sb.isFpFlag()) {

						RunSimulation.memoCache.put(sb.getMemAddress(),sb.getFpval());

					}
					else if (!sb.isFpFlag()) {
						RunSimulation.memoCache.put(sb.getMemAddress(),(double)sb.getIntval());

					}


				}

			}
			// for non-store word
			else{
				// UPDATE REGISTERS
				// if fp
				if (entry.getFP()) {
					// update FP registers according to instruction
					if (entry.getwbReg() != null && !entry.getwbReg().equals("EMPTY")){
						RunSimulation.registers.writeFPReg(entry.getwbReg(),entry.getfpVal());
						//System.out.println("%%%%%%%%%% FPval "+entry.getfpVal()+"to "+entry.getwbReg());
						//System.out.println("%%%%%%%%%% ival "+entry.getiVal()+"to "+entry.getwbReg());
					}
					//update val to replace ROB# with in other instr
					val = String.valueOf(entry.getfpVal());
				}
				// if int
				else {
					// update int registers according to instruction
					if (entry.getwbReg() != null && !entry.getwbReg().equals("EMPTY")){
						RunSimulation.registers.writeIntReg(entry.getwbReg(),entry.getiVal());
						//System.out.println("%%%%%%%%%%fp val "+entry.getfpVal()+"to "+entry.getwbReg());
						//System.out.println("%%%%%%%%%% ival "+entry.getiVal()+"to "+entry.getwbReg());
					}
					//update val to replace ROB# with in other instr
					val = ""+entry.getiVal();
				}
			}
			// REMOVE (everything should be updated in remove function)
			remove();

			// REPLACE from new first-last everywhere ROB# is with value as string
			// functional unit should test these values and execute based on that
			if (num > 0) {			
				boolean reachedLast = false;
				int currentInd = first;
				while(!reachedLast) {
					RBEntry e = a[currentInd];
					if (e != null) {
						Instruction in = e.inst;
						if (in != null) {
							String sr1 = in.getrSrc1();
							String sr2 = in.getrSrc2();
							if (sr1 != null && sr1.equals(robToReplace)) {
								a[currentInd].inst.setrSrc1(val);
							}
							if (sr2 != null && sr2.equals(robToReplace)) {
								a[currentInd].inst.setrSrc2(val);
							}
							if (currentInd == last) {
								reachedLast = true;	
							}
							else { 
								currentInd = getNextIndex(currentInd); 
							}
						}
					}

				}
			}
			
			return true;
		}
		return false;
	}
	


	/************************* TO SET VALS AND FLAGS *********************************/
	
	// set reorder buffer flag to true for particular ROB
	public void setwbFlagTrue(String currentROB) {
		if (currentROB == null ) return;
		//System.out.println("&&&&&&&&& wb flag set for "+ currentROB);
		Pattern memoLoc = Pattern.compile("\\d+");
		Matcher match_memo = memoLoc.matcher(currentROB);
		if(match_memo.find()){
			int index = Integer.parseInt(match_memo.group(0));

			// set to true	
			a[index].setwbFlag(true);
		}		

	}

	// set Int value in reorderbuffer for particular ROB
	public void setIntVal(String currentROB, int value) {
		if (currentROB == null ) return;

		Pattern memoLoc = Pattern.compile("\\d+");
		Matcher match_memo = memoLoc.matcher(currentROB);
		if(match_memo.find()){
			int index = Integer.parseInt(match_memo.group(0));

			// get instruction to replace			
			a[index].setiVal(value);
		}


	}


	// set FP value in reorderbuffer for particular ROB
	public void setFPVal(String currentROB, double value) {
		if (currentROB == null ) return;

		Pattern memoLoc = Pattern.compile("\\d+");
		Matcher match_memo = memoLoc.matcher(currentROB);
		if(match_memo.find()){
			int index = Integer.parseInt(match_memo.group(0));

			// get instruction to replace			
			a[index].setfpVal(value);
		}


	}


	// retrieve String value from Src1, returns null if stored as null
	public String getSrc1(String currentROB) {
		if (currentROB == null ) return null;

		Pattern memoLoc = Pattern.compile("\\d+");
		Matcher match_memo = memoLoc.matcher(currentROB);
		if(match_memo.find()){
			int index = Integer.parseInt(match_memo.group(0));

			// get instruction to replace			
			Instruction currentInstr = a[index].getInstruction();	
			String sr1 = currentInstr.getrSrc1();	
			return sr1;
		}
		return null;			
	}

	// retrieve String value from Src2, returns null if stored as null
	public String getSrc2(String currentROB) {
		if (currentROB == null ) return null;

		Pattern memoLoc = Pattern.compile("\\d+");
		Matcher match_memo = memoLoc.matcher(currentROB);
		if(match_memo.find()){
			int index = Integer.parseInt(match_memo.group(0));

			// get instruction to replace			
			Instruction currentInstr = a[index].getInstruction();	
			String sr2 = currentInstr.getrSrc2();	
			return sr2;
		}
		return null;	

	}
	/**************************************************************************************/


	// prints everything in order (first to last) or index (0 to size-1)
	// must use printAllContents("order") or printAllContents("index")
	public void printAllContents(String s) {
		if (s.equals("order")) { printAllContents(); }
		else if (s.equals("index")) {
			System.out.println("\n**** PRINTING CONTENTS OF REORDER BUFFER BY INDEX ****");
			for (int i = 0; i < size; i++) {
				RBEntry entry = a[i];
				if (entry != null) {
					System.out.println("index: "+i);
					System.out.println("Instr orig: "+entry.inst.getContent());
					System.out.println("Instr: "+entry.inst.getOperation()+ " rdest: "+entry.inst.getrDest()+" rsrc1: "+entry.inst.getrSrc1()+" rsrc2: "+entry.inst.getrSrc2());
					System.out.println("wbReg: "+entry.getwbReg());
					System.out.println("fp: "+entry.getFP());
					System.out.println("iVal: "+entry.getiVal());
					System.out.println("fpVal: "+entry.getfpVal());
					System.out.println("");
				}
				/*
				else {
					System.out.println("index: "+i);
					System.out.println("EMPTY\n");
				}
				*/

			}
		
		}

	}
	

	

}
