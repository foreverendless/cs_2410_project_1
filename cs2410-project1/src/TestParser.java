import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;


public class TestParser {
	public static void main (String[] args) {
		Parser test = new Parser();
		ArrayList<Instruction> instCache = new ArrayList<Instruction>();
		HashMap<Integer, Double> memoCache = new HashMap<Integer, Double>();
		HashMap<String, Integer> branchLoc = new HashMap<String, Integer>();
		try {
			test.read(new FileInputStream("benchmark1.dat"));
			instCache = test.returnInstCache();
			memoCache = test.returnMemoryCache();
			branchLoc = test.returnBranchLoc();
			
			System.out.println("Instruction cache: ");
			// display the items in each cache
			for(Instruction i:instCache){
				System.out.println(i.getContent());
			}
			System.out.println("\nMemory map:\n"+memoCache.toString());
			System.out.println("\nBranch map:\n"+branchLoc.toString());
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
}
