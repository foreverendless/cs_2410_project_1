import java.util.*;

public class BranchTargetBuffer {

  private HashMap<Integer, BufferLine> btb;
  
  // CONSTRUCTOR
  public BranchTargetBuffer() {
    HashMap<Integer, BufferLine> btb = new HashMap<Integer, BufferLine>();
    for (int i = 0; i < 32; i++) {
      btb.put(i, new BufferLine());
    }
  }
  
  // METHODS
  private Integer bufferLineNumber(Integer inst) {
    return inst % 32;
  }
  
  // Note: The full instruction number, inst, serves as the tag.
  public Integer getTarget(Integer inst) {
    BufferLine bl = btb.get(bufferLineNumber(inst));
    return bl.getTarget(inst);
  }
  public void setTarget(Integer inst, Integer target) {
    BufferLine bl = btb.get(bufferLineNumber(inst));
    bl.set(inst, target);
  }
  
  
  // INNER CLASS
  private class BufferLine {
    // Each buffer line contains a tag and target.
    private Integer tag;
    private Integer target;
    
    public BufferLine() {
      // Create an uninitialized buffer line.
      set(null, null);
    }
    public BufferLine(Integer tag, Integer target) {
      // Create an initialized buffer line.
      set(tag, target);
    }
    
    public void set(Integer tag, Integer target) {
      this.tag = tag;
      this.target = target;
    }
    public boolean tagMatch(Integer t) {
      return tag.equals(t);
    }
    public Integer getTarget(Integer t) {
      // Return target if tags match; null otherwise.
      if (tagMatch(t)) { return target; }
      else { return null; }
    }
    
  }

}
