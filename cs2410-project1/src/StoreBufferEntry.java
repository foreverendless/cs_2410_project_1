import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class StoreBufferEntry {
	private int memAddress;
	private boolean fpFlag; // true if fp, false if int
	private double fpval;
	private int intval;
	
	public int getMemAddress() {
		return memAddress;
	}
	public void setMemAddress(int memAddress) {
		this.memAddress = memAddress;
	}
	public boolean isFpFlag() {
		return fpFlag;
	}
	public void setFpFlag(boolean fpFlag) {
		this.fpFlag = fpFlag;
	}
	public double getFpval() {
		return fpval;
	}
	public void setFpval(double fpval) {
		this.fpval = fpval;
	}
	public int getIntval() {
		return intval;
	}
	public void setIntval(int intval) {
		this.intval = intval;
	}
	
}
