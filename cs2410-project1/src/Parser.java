import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Parser {
	HashMap<Integer, Double> memoMap = new HashMap<Integer, Double>();
	HashMap<String, Integer> branchLoc = new HashMap<String, Integer>();
	ArrayList<Instruction> instList = new ArrayList<Instruction>();
	
	String op0, op1, op2;

	public void read(FileInputStream fis) {
		Scanner reader = new Scanner(fis);
		int countAddr = 0;
		
		while (reader.hasNext()) {
			String s = reader.nextLine();
			// Detect instruction
			Pattern instru = Pattern.compile("[A-Z]+(\\.|)[A-Z]+");
			Matcher match_inst = instru.matcher(s);
			if(match_inst.find()) {
				// Use regex to match operations
				Instruction inst = new Instruction();
				inst.setOperation(match_inst.group(0));
				// Remove operation part in the instruction
				inst.setOperands(s.replaceAll(inst.getOperation(), ""));
				if(inst.getOperation().equals("DATA")) {
					continue;
				}
				// Remove all whitespaces and tabs in the string
				inst.setOperands(inst.getOperands().replaceAll("\\t+", ""));
				inst.setOperands(inst.getOperands().replaceAll("\\s+", ""));
				// record loop alias
				if(inst.getOperands().substring(0, 5).equals("loop:")) {
					inst.setAlias("loop");
					inst.setOperands(inst.getOperands().replaceAll("loop:", ""));
					branchLoc.put("loop", countAddr);
				} else if(inst.getOperands().substring(0, 6).equals("loop1:")) {
					inst.setAlias("loop1");
					inst.setOperands(inst.getOperands().replaceAll("loop1:", ""));
					branchLoc.put("loop1", countAddr);
				} else if(inst.getOperands().substring(0, 6).equals("loop2:")) {
					inst.setAlias("loop2");
					inst.setOperands(inst.getOperands().replaceAll("loop2:", ""));
					branchLoc.put("loop2", countAddr);
				}
				// add the instruction to the instruction cache
				inst.setAddress(countAddr);
				countAddr++;
				// switch
				if(inst.getOperation().equals("ORI")){
					ori(inst);
				} else if ((inst.getOperation().equals("L.D"))||(inst.getOperation().equals("LD"))){
					ldotD(inst);
				} else if (inst.getOperation().equals("MUL.D")){
					mulD(inst);
				} else if (inst.getOperation().equals("ADD.D")){
					addD(inst);
				} else if ((inst.getOperation().equals("S.D"))||(inst.getOperation().equals("SD"))){
					sdotD(inst);
				} else if (inst.getOperation().equals("DADDI")){
					ori(inst);
				} else if (inst.getOperation().equals("BNEZ")){
					bnez(inst);
				} else if (inst.getOperation().equals("SUB.D")){
					addD(inst);
				} else if (inst.getOperation().equals("DMUL")){
					addD(inst);
				} else if (inst.getOperation().equals("DIV.D")){
					mulD(inst);
				} else if (inst.getOperation().equals("DADD")){
					addD(inst);
				} else if (inst.getOperation().equals("DSUB")){
					addD(inst);
				} else if (inst.getOperation().equals("AND")){
					addD(inst);
				} else if (inst.getOperation().equals("ANDI")){
					ori(inst);
				} else if (inst.getOperation().equals("OR")){
					addD(inst);
				} else if (inst.getOperation().equals("SLT")){
					addD(inst);
				} else if (inst.getOperation().equals("SLTI")){
					ori(inst);
				} else if (inst.getOperation().equals("BEQZ")){
					beq(inst);
				} else if (inst.getOperation().equals("BEQ")){
					beq(inst);
				} else if (inst.getOperation().equals("BNE")){
					beq(inst);
				}
				instList.add(inst);
			} else if(s.contains("Mem")) {
				s = s.replaceAll("\\s+", "");
				Pattern key = Pattern.compile("\\d+");
				Matcher match_loc = key.matcher(s);
				Pattern value = Pattern.compile("\\d+");
				Matcher match_val = value.matcher(s.substring(7));
				if((match_loc.find())&&(match_val.find())){
					// put memory location and value into a HashMap
					memoMap.put(Integer.parseInt(match_loc.group(0)),Double.parseDouble(match_val.group(0)));
				}
			}
		}
	}
	
	public HashMap<Integer, Double> returnMemoryCache() {
		return memoMap;
	}
	
	public ArrayList<Instruction> returnInstCache() {
		return instList;
	}
	
	public HashMap<String, Integer> returnBranchLoc() {
		return branchLoc;
	}
	
	/* 
	 * ORI, DADDI, SLTI, ANDI: address. rDest, rSrc1, immediate
	 * fuCount=1
	 */
	private void ori(Instruction inst) {
		String op = inst.getOperands();
		inst.setrDest(op.split(",")[0]);
		inst.setrSrc1(op.split(",")[1]);
		inst.setImmediate(Integer.parseInt(op.split(",")[2]));
		inst.setFuCount(1+1);
		inst.setFuncUnit("XSU");
	}
	
	/* 
	 * L.D (LD): address. rDest, offset(rSrc1)
	 * default fuCount=2, update to 7 later if there's a cache miss 
	 */
	private void ldotD(Instruction inst) {
		String op = inst.getOperands();
		inst.setrDest(op.split(",")[0]);
		Pattern memoLoc = Pattern.compile("\\d+");
		Matcher match_memo = memoLoc.matcher(op.split(",")[1]);
		Pattern src = Pattern.compile("R\\d");
		Matcher match_src = src.matcher(op.split(",")[1]);
		if((match_memo.find())&&(match_src.find())){
			inst.setOffset(Integer.parseInt(match_memo.group(0)));
			inst.setrSrc1(match_src.group(0));
		}
		inst.setFuCount(2+1);
		inst.setFuncUnit("LS");
	}
	
	/*
	 * MUL.D: address. rDest, rSrc1, rSrc2
	 * fuCount=3
	 * DIV.D: address. rDest, rSrc1, rSrc2
	 * fuCount=32
	 */
	private void mulD(Instruction inst) {
		String op = inst.getOperands();
		inst.setrDest(op.split(",")[0]);
		inst.setrSrc1(op.split(",")[1]);
		inst.setrSrc2(op.split(",")[2]);
		if(inst.getOperation().equals("DIV.D")){
			inst.setFuCount(32);
		} else {
			inst.setFuCount(3+1);
		}
		inst.setFuncUnit("FPU");
	}
	
	/*
	 * ADD.D, SUB.D: address. rDest, rSrc1, rSrc2
	 * fuCount=3
	 * DMUL: address. rDest, rSrc1, rSrc2
	 * fuCount=4
	 * SLT, OR, AND, DSUB, DADD: address. rDest, rSrc1, rSrc2
	 * fuCount=1
	 */
	private void addD(Instruction inst) {
		String op = inst.getOperands();
		inst.setrDest(op.split(",")[0]);
		inst.setrSrc1(op.split(",")[1]);
		inst.setrSrc2(op.split(",")[2]);
		if(inst.getOperation().equals("DMUL")){
			inst.setFuCount(4);
			inst.setFuncUnit("MCF");
		} else if ((inst.getOperation().equals("ADD.D"))||(inst.getOperation().equals("SUB.D"))){
			inst.setFuCount(3);
			inst.setFuncUnit("FPU");
		} else if ((inst.getOperation().equals("SLT"))||(inst.getOperation().equals("OR"))||
					(inst.getOperation().equals("AND"))||(inst.getOperation().equals("DADD"))||
					(inst.getOperation().equals("DSUB"))){
			inst.setFuCount(1+1);
			inst.setFuncUnit("XSU");
		}
	}
	
	/*
	 * S.D (SD): address. rSrc1, offset, rSrc2
	 * fuCount=1
	 */
	private void sdotD(Instruction inst) {
		String op = inst.getOperands();
		inst.setrSrc1(op.split(",")[0]);
		Pattern memoLoc = Pattern.compile("\\d+");
		Matcher match_memo = memoLoc.matcher(op.split(",")[1]);
		Pattern src = Pattern.compile("R\\d");
		Matcher match_src = src.matcher(op.split(",")[1]);
		if((match_memo.find())&&(match_src.find())){
			inst.setOffset(Integer.parseInt(match_memo.group(0)));
			inst.setrSrc2(match_src.group(0));
		}
		inst.setFuCount(1+1);
		inst.setFuncUnit("LS");
	}
	
	/*
	 * BNEZ: address. rSrc1, rSrc2
	 * fuCount=1
	 */
	private void bnez(Instruction inst) {
		String op = inst.getOperands();
		inst.setrSrc1(op.split(",")[0]);
		inst.setrSrc2(op.split(",")[1]);
		inst.setFuCount(1+1);
		inst.setFuncUnit("BPU");
	}
	
	/*
	 * BEQ: address. rSrc1, rSrc2, offset
	 * fuCount=1
	 * BNE: address. rSrc1, rSrc2, offset
	 * fuCount=1
	 * BEQZ: address. rSrc1, rSrc2, offset
	 * fuCount=1
	 */
	private void beq(Instruction inst) {
		String op = inst.getOperands();
		inst.setrSrc1(op.split(",")[0]);
		inst.setrSrc2(op.split(",")[1]);
		inst.setOffset(Integer.parseInt(op.split(",")[2]));
		inst.setFuCount(1+1);
		inst.setFuncUnit("BPU");
	}
}
