import java.util.*;

public class Register {

	private boolean rename;
	
    // integer registers
	private HashMap<String, Integer> IntRegs;
	// floating point registers
	private HashMap<String, Double> FPRegs;
    
	// initialize all to 0
	public Register() {
		rename = true;
		IntRegs = new HashMap<String, Integer>();
	    FPRegs = new HashMap<String, Double>();
		initAll();
	}
	
	// initialize all to 0, rename is optional
	public Register(boolean b) {
		rename = b;
		IntRegs = new HashMap<String, Integer>();
	    FPRegs = new HashMap<String, Double>();
		initAll();
	}

	private void initAll() {
		StringBuilder Rx = new StringBuilder("R");
		StringBuilder Fx = new StringBuilder("F");
		for (int i = 0; i < 32; i++) {
			Rx.append(i);
			Fx.append(i);
			IntRegs.put(Rx.toString(), 0);
			FPRegs.put(Fx.toString(), 0.0);
			if(i<10){
				Rx.deleteCharAt(1);
				Fx.deleteCharAt(1);
			} else{
				Rx.delete(1, 3);
				Fx.delete(1, 3);
			}
		}
	}
	
	public void printInt() {
		System.out.println("******** Integer Registers **********");
		System.out.println(IntRegs.toString());

	}
	
	public void printFP() {
		System.out.println("******** FP Registers ***************");
		System.out.println(FPRegs.toString());	
	}
	
	public void printAll() {
		printInt();
		printFP();
	}
	
	// read and write registers
	public int readIntReg(String Rx) {
		return IntRegs.get(Rx);
	}
	
	public void writeIntReg(String Rx, int value) {
		IntRegs.put(Rx, value);
	}

	public double readFPReg(String Fx) {
		return FPRegs.get(Fx);
	}
	
	public void writeFPReg(String Fx, double value) {
		FPRegs.put(Fx, value);
	}
	
	public void test() {
		System.out.println("------------- REGISTER TEST ----------");
		
		printAll();
		//Tester.sb.printRegs();
	}
}

