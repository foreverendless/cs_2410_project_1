import java.util.*;

public class RsArray {

	private ArrayList<Instruction> a;
	private int c;
	private int indEx;

	public RsArray(int capacity) {
		a = new ArrayList<Instruction>();
		c = capacity;

	}
	
	public boolean hasInst() {
		return a.size()>0;
	}
	
	// returns true or false value to see if instr can be added
	public boolean hasSpace() {
		if (a.size() < c) return true;
		return false;
	}

	// returns the number of instr that can be added 
	public int spaceAvailable() {
		return (c - a.size());
	}

	// returns the capacity of the reservation station array	
	public int capacity() {
		return c;
	}

	// add an instruction. returns false if failed
	public boolean add(Instruction n) {
		boolean added = false;
		if (a.size() < c) {
			a.add(n);
			added = true;
		}

		return added;
	}

	// set instr to execute by index
	public boolean execute(int i) {
		if (i < c && i >= 0) {
			indEx = i;
			return true;
		}
		return true;

	}

	// set inst to execute by instr
	public int execute(Instruction inst) {
		int i = a.indexOf(inst);		
		if (i >= 0) indEx = i;
		return i;
		
	}

	// arrayList methods
	public void clear() {
		a.clear();
	}

	public int size() {
		return a.size();
	}	

	public boolean contains(Instruction n) {
		return a.contains(n);
	}

	public Instruction get(int index) {
		return a.get(index);
	}

	public int indexOf(Instruction n) {
		return a.indexOf(n);
	}

	public Instruction remove(int index) {
		return a.remove(index);
	}

	public boolean remove(Instruction n) {
		return a.remove(n);
	}


}
