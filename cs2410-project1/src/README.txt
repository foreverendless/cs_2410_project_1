Chelsea Mafrica (cem37), Song Jin
CS2410 Project 1
October 16, 2013

Java version:
	java version "1.7.0_09"

compile: 
	javac *.java


run without input parameters: 
	java RunSimulation benchmarkX.dat

run with input parameters:
	java RunSimulation benchmark NF NQ ND NW NR
	or
	java RunSimulation benchmark NF NQ ND NW NR true/false

	NF = max num of instr fetched (default 4)
	NQ = max num of instr queue (default 8)
	ND = max num instr decoded and put into Issue Q (default 4)
	NW = max num of instr that can be issued (default 4)
	NR = number of reorder buffers (default 16)
	true/false value is for branch prediction with target buffer, default false (optional)

output:
	Each cycle, the contents of the reorder buffer, functional units, issue queue, and 
	instruction queue is printed. The contents of registers and memory is also printed.





