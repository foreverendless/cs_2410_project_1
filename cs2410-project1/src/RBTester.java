import java.util.*;
import java.io.*;

public class RBTester {

	public static void main(String[] args) {
                ArrayList<Instruction> instCache = new ArrayList<Instruction>(); // i-cache
                HashMap<Integer, Double> memoCache = new HashMap<Integer, Double>(); // data cache

                Parser parser = new Parser();
                try {
                        parser.read(new FileInputStream("benchmark1.dat"));
                        instCache = parser.returnInstCache();
                        memoCache = parser.returnMemoryCache();
                }
                catch (FileNotFoundException e) {
                        e.printStackTrace();
                        System.exit(1);
                }
		ReorderBuffer rob = new ReorderBuffer(4);
		System.out.println("START");
		System.out.println("reorder buffer has space: "+rob.hasSpace());
		System.out.println("reorder buffer has this much space: "+rob.spaceAvailable());
		rob.printStats();
		System.out.println();

		for (int i = 0; i < 6; i++) {
			System.out.println("TRY TO ADD INST FROM ICACHE "+i);
			Instruction INS = instCache.get(i);
			if (rob.hasSpace()) {
				System.out.println(rob.add(INS));
			}
			System.out.println("reorder buffer has space: "+rob.hasSpace());
			System.out.println("reorder buffer has this much space: "+rob.spaceAvailable());
			rob.printStats();
			System.out.println();
		}

		rob.remove();

		rob.printAllContents("index");

		System.out.println("\n");
		rob.printAllContents("order");

		// clear from index 2 to last
		rob.clear(2);

		rob.printAllContents("index");
		rob.printStats();
		
	}




	
}
