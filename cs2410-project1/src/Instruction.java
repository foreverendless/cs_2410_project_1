
public class Instruction {
	private int address;
	private String alias;
	private String operation;
	private String operands;
	
	// for display purpose
	private String content;

	// instr with ALU operands
	private String rDest; 
	private String rSrc1;
	private String rSrc2;
	private int immediate;
	
	// memory locations for load & store operands
	private int offset;
	
	// how many cycles does this inst take 
	// in a specific func unit
	private int fuCount;
	private String funcUnit;
	
	public int getAddress() {
		return address;
	}
	public void setAddress(int address) {
		this.address = address;
	}
	public String getAlias() {
		return alias;
	}
	public void setAlias(String alias) {
		this.alias = alias;
	}
	public String getOperation() {
		return operation;
	}
	public void setOperation(String operation) {
		this.operation = operation;
	}
	public String getrDest() {
		return rDest;
	}
	public void setrDest(String rDest) {
		this.rDest = rDest;
	}
	public String getrSrc1() {
		return rSrc1;
	}
	public void setrSrc1(String rSrc1) {
		this.rSrc1 = rSrc1;
	}
	public String getrSrc2() {
		return rSrc2;
	}
	public void setrSrc2(String rSrc2) {
		this.rSrc2 = rSrc2;
	}
	public int getOffset() {
		return offset;
	}
	public void setOffset(int offset) {
		this.offset = offset;
	}
	public int getImmediate() {
		return immediate;
	}
	public void setImmediate(int immediate) {
		this.immediate = immediate;
	}
	public int getFuCount() {
		return fuCount;
	}
	public void setFuCount(int fuCount) {
		this.fuCount = fuCount;
	}
	public String getOperands() {
		return operands;
	}
	public void setOperands(String operands) {
		this.operands = operands;
	}
	public String getContent() {
		return getOperation()+": "+getrDest()+","+getrSrc1()+","+getrSrc2()+" imme:"
				+getImmediate()+"/off:"+getOffset()+"/cycles:"+getFuCount()+"/func:"+getFuncUnit()+
				"/alias:"+getAlias();
	}
	public String getFuncUnit() {
		return funcUnit;
	}
	public void setFuncUnit(String funcUnit) {
		this.funcUnit = funcUnit;
	}
}
