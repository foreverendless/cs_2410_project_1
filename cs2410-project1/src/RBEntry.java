import java.util.*;


public class RBEntry {
	// each entry has an instruction, wb register, flag for fp, and value
	public Instruction inst;
	public String wbReg;
	public boolean fp;
	public int iVal;
	public double fpVal;

	public boolean wbFlag;
	
	public RBEntry(Instruction n) {
		inst = n;
		setFP();
		setwbReg();
		setwbFlag(false);
	}
	
	public void setwbFlag(boolean flag) {
		wbFlag = flag;
	}
	
	public Instruction getInstruction() {
		return inst;
		
	}

	public void resetInstr(Instruction i) {
		i = inst;
		
	}

	public String getwbReg() {
		return wbReg;

	}

	public boolean getFP() {
		return fp;
	}

	public int getiVal() {
		return iVal;
	}

	public double getfpVal() {
		return fpVal;
	}

	public void setiVal(int i) {
		iVal = i;
	}
	public void setfpVal(double f) {
		fpVal = f;
	}

	private void setwbReg() {
		String fu = inst.getFuncUnit();
		if (fu.equals("XSU")) {
			wbReg = inst.getrDest();
		}
		else if (fu.equals("LS") && (inst.getOperation().equals("LD") || inst.getOperation().equals("L.D") )) {
			wbReg = inst.getrDest();
		}
		else if (fu.equals("FPU")) {
			wbReg = inst.getrDest();
		}
		else if (fu.equals("MCF")) {
			wbReg = inst.getrDest();
		}
		else if (fu.equals("BPU")) {
			wbReg = null;
		}
		else { wbReg = null; }
		return;
	}
	// set fp flag to true if operation contains "." (indicates fp operation)
	private void setFP() {
		boolean isFP = false;
		if (inst.getOperation().contains(".")) isFP = true;
		fp = isFP;
		return;
	}
	
}

