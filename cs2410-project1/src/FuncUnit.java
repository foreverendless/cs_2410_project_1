import java.util.HashMap;


public class FuncUnit {
	
	Registers callRegMap = new Registers(true);
	
	// execution in XSU
	public int executeXSU(Instruction inst) {
		int result=0, imme=inst.getImmediate();
		String op = inst.getOperation();
		int valRx = Integer.parseInt(inst.getrSrc1());
		
		// perform different execution to each operation
		// only implement functions which exist in the benchmark
		if(op.equals("ORI")||op.equals("DADDI")){
			result = imme+valRx;
		} else if(op.equals("SLTI")) {
			System.out.println(op+" is not supported in this program");
		} else if(op.equals("ANDI")) {
			System.out.println(op+" is not supported in this program");
		} else if(op.equals("SLT")){
			System.out.println(op+" is not supported in this program");
		} else if(op.equals("OR")){
			System.out.println(op+" is not supported in this program");
		} else if(op.equals("AND")){
			System.out.println(op+" is not supported in this program");
		} else if(op.equals("DADD")){
			System.out.println(op+" is not supported in this program");
		} else if(op.equals("DSUB")){
			System.out.println(op+" is not supported in this program");
		} 
		return result;
	}
	
	// execution in MCFXU
	public int executeMCF(Instruction inst) {
		int result=0;
		String op = inst.getOperation();
		int valRx = Integer.parseInt(inst.getrSrc1());
		int valRx2 = Integer.parseInt(inst.getrSrc2());
		result = valRx * valRx2;
		return result;
	}
	
	// execution in LOAD/STORE
	// need to specify L.D/S.D in the main program
	public double executeFPLS(Instruction inst, HashMap<Integer, Double> memory) {
		
		// sample: L.D F0,0(R1)
		String op = inst.getOperation();
		int valRx = 0;
		double result=0.0;
		int offset = inst.getOffset();
		if(op.equals("L.D")){
			valRx = Integer.parseInt(inst.getrSrc1());
			// test to see if key exists, exit if not
			boolean keyExists = memory.containsKey(offset+valRx);
			if (keyExists) {
				result = memory.get(offset+valRx);
			}
			else {
				System.out.println("\nKEY DOES NOT EXIST IN MEMORY HASH MAP");
				System.out.println("CRASH");
				System.exit(0);
			}
			//System.out.println("&&&&&&&&"+result);
			return result;
		} else if(op.equals("S.D")){
			valRx = Integer.parseInt(inst.getrSrc2());
			return offset+valRx;
		}
		return 0.0;
	}
	
	// need to specify LD/SD in the main program
	public int executeINTLS(Instruction inst, HashMap<Integer, Double> memory) {

		// sample: LD R1,0(R2)
		String op = inst.getOperation();
		int valRx = 0, result=0;
		int offset = inst.getOffset();
		if(op.equals("LD")){
			valRx = Integer.parseInt(inst.getrSrc1());
			result = memory.get(offset+valRx).intValue();
			return result;
		} else if(op.equals("SD")){
			valRx = Integer.parseInt(inst.getrSrc2());
			return offset+valRx;
		}
		return 0;
	}
	
	// execution in FPU
	public double executeFPU(Instruction inst) {
		
		// sample: ADD.D F0,F0,F4
		double result=0;
		String op = inst.getOperation();
		double valRx1 = Double.parseDouble(inst.getrSrc1());
		double valRx2 = Double.parseDouble(inst.getrSrc2());
		if(op.equals("ADD.D")){
			result = valRx1+valRx2;
		} else if(op.equals("SUB.D")){
			result = valRx1-valRx2;
		} else if(op.equals("MUL.D")){
			result = valRx1*valRx2;
		} else if(op.equals("DIV.D")){
			result = valRx1/valRx2;
		}
		return result;
	}
	
	// execution in BPU
	public boolean executeBPU(Instruction inst) {
		boolean flag = false; // not taken
		String op = inst.getOperation();
		int valRx = Integer.parseInt(inst.getrSrc1());
		if(op.equals("BNEZ")){
			// sample: BNEZ R1,loop
			if(valRx!=0){
				flag = true;
			} else {
				flag = false;
			}
		} else if(op.equals("BEQZ")) {
			System.out.println(op+" is not supported in this program");
		} else if(op.equals("BEQ")) {
			System.out.println(op+" is not supported in this program");
		} else if(op.equals("BNE")) {
			System.out.println(op+" is not supported in this program");
		} 
		return flag;
	}
}
