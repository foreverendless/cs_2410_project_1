import java.util.*;

public class ArrayQueue<E> extends AbstractQueue<E>
							implements Queue<E>{
	private int front;
	private int rear;
	private int size;
	private int capacity;
	private E[] theData;
	
	@SuppressWarnings("unchecked")
	public ArrayQueue(int num) {
		this.capacity = num;
		theData = (E[]) new Object[capacity];
		front = 0;
		rear = capacity - 1;
		size = 0;
	}

	@Override
	public boolean offer(E item) {
		if(size==capacity){
			return false;
		}
		size++;
		rear = (rear+1) % capacity;
		theData[rear] = item;
		return true;
	}

	@Override
	public E peek() {
		if(size==0){
			return null;	
		}
		return theData[front];
	}

	@Override
	public E poll() {
		E value;
		if(size==0){
			return null;
		}
		value = theData[front];
		front = (front+1) % capacity;
		size--;
		return value;
	}

	@Override
	public int size() {
		return size;
	}
	
	@Override
	public Iterator<E> iterator() {
		return new Iter();
	}
	
	@Override
	public String toString() {
		StringBuilder output = new StringBuilder();
		Iterator<E> iter = iterator();
		while(iter.hasNext()){
			output.append(iter.next()+" ");
		}
		return output.toString();
	}

	private class Iter implements Iterator<E> {
		private int index;
		private int count = 0;
		
		public Iter() {
			index = front;
		}

		@Override
		public boolean hasNext() {
			return count < size;
		}

		@Override
		public E next() {
			if(!hasNext()){
				throw new NoSuchElementException();
			}
			E value = theData[index];
			index = (index+1) % capacity;
			count++;
			return value;
		}

		@Override
		public void remove() {
			throw new UnsupportedOperationException();
		}	
	}
	
}
