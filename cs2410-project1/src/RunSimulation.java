import java.util.*;
import java.io.*;

public class RunSimulation {

        /****** variables for statistics ******/
	/// arraylists store values retrieved each cycle 
	// resrvation stations, reorder buffers
	public static ArrayList<Integer> rbrsStats; //stores overall num of reorder buffers/reservation stations used
	// renames
	public static ArrayList<Integer> intrbStats; // stores number of ROBs used for int
	public static ArrayList<Integer> fprbStats; // stores number of ROBs used for fp
	// CDBs
	public static ArrayList<Integer> nwStats; // stores count of num issued per cycle
	public static ArrayList<Integer> wbStats; // counts number written back per cycle
	public static int wrongPredT;
	public static int wrongPredNT;	
	public static int rightPredNT;	
	public static int rightPredT;	
	public static ArrayList<Integer> forwardStats; // counts number of instr with value used in reservation stations

	
        /********** input variables ***********/
		private final static int DESIRED_RUNNING_CYCLES = 300;
	
        public static boolean branchPred; // branch prediction
        // PER CLOCK CYCLE
        public static int NF; // max num of instr fetched
        public static int NQ; // max num of instr queue
        public static int ND; // max num instr decoded and put into Issue Q
        public static int NI; // max num of instr in instruction queue
        public static int NW; // max num of instr that can be issued
        public static int NR; // number of reorder buffers

        /********** other variables ***********/
        public static int PC; // set PC to 0
        public static boolean updatedPC;
        public static int globalCycles; // overall number of cycles
        public static String benchmark;
        
        public static Registers registers;
        public static ArrayList<Instruction> instCache = new ArrayList<Instruction>(); // i-cache
        public static HashMap<Integer, Double> memoCache = new HashMap<Integer, Double>(); // data cache
        public static HashMap<String, StoreBufferEntry> linkToSDEntry = new HashMap<String, StoreBufferEntry>(); // structure needed to handle S.D       
        
        /********** set default variables, input and other *******/
        public static void setDefaultVars() {
                // input vars
                NF = 4;
                NQ = 8;
                ND = 4;
                NI = 8;
                NW = 4;
                NR = 16;
                branchPred = false;
                
                // other vars
                PC = 0;
                globalCycles = 0;
                updatedPC = false;
        
        }
        public static void setInputVars(String[] args) {
                // input vars
                benchmark = args[0];
                NF = Integer.parseInt(args[1]);
                NQ = Integer.parseInt(args[2]);
                ND = Integer.parseInt(args[3]);
                NI = Integer.parseInt(args[2]); // same as NQ
                NW = Integer.parseInt(args[4]);
                NR = Integer.parseInt(args[5]);
                if (args.length == 7) {
                        if (args[6].equals("f") || args[6].equals("false")) {
                                branchPred = false;
                        }
                        else {
                                branchPred = true;
                        }
                }
                else {
                        branchPred = false;
                }
                
                // other vars
                PC = 0;
                globalCycles = 0;
                updatedPC = false;
        }
        
        /***************** PRINT STATEMENTS FOR DEBUGGING *************/
        public static void printPC() {
                System.out.println("PC: "+PC);
        }
        public static void printCycles() {
                System.out.println("globalCycles: "+globalCycles);
        }
        public static void printAllVars() {
                System.out.println("**** Variable values *****");
                System.out.println("benchmark: "+benchmark);
                System.out.println("NF: "+NF);
                System.out.println("NQ: "+NQ);
                System.out.println("ND: "+ND);
                System.out.println("NI: "+NI);
                System.out.println("NW: "+NW);
                System.out.println("NR: "+NR);
                System.out.println("branchPred: "+branchPred);
                printPC();
                printCycles();
                return;
        }
        
        
        /******************** main for simulation ***************/
        public static void main(String[] args) {
                
                // set default variables 
                setDefaultVars();
                
                // get command line args, if there are any
                if (args.length == 6 || args.length == 7) {
                        setInputVars(args);
                }
                else if (args.length != 1) {
                        System.out.println("\nEnter input in following order:");
                        System.out.println("java RunSimulation benchmark");
                        System.out.println("or");
                        System.out.println("java RunSimulation benchmark NF NQ ND NW NR true/false");
                        System.out.println("Note: true/false is for branch prediction and is optional");
                        System.exit(1);
                }
                else {
                        benchmark = args[0];
                }
                
                ////// INITIALIZE ALL DATA STRUCTURES HERE ////////////////////////
                FuncUnit execUnit = new FuncUnit();
                registers = new Registers(false);
                
                // instruction queue
                ArrayQueue<Instruction> inst_queue = new ArrayQueue<Instruction>(NQ);
                // issue queue
                ArrayQueue<Instruction> issue_queue = new ArrayQueue<Instruction>(NI);
                // instructions needed to be flushed are stored in this list
                ArrayList<String> flush_list = new ArrayList<String>();
                // functional units
                RsArray xsu0 = new RsArray(2);
                RsArray xsu1 = new RsArray(2);
                RsArray mcfxu = new RsArray(5);
                RsArray ls = new RsArray(5);
                RsArray fpu = new RsArray(5);
                RsArray bpu = new RsArray(2);
                // reorder buffer
                ReorderBuffer rob = new ReorderBuffer(NR);
		  		// branch target buffer - initialize to -1
				Integer[] branchTargetBuffer = new Integer[32];
				for (int i = 0; i < branchTargetBuffer.length; i++) {
					branchTargetBuffer[i] = -1;
				}              
		// statistics
		rbrsStats = new ArrayList<Integer>();
		intrbStats = new ArrayList<Integer>();
		fprbStats = new ArrayList<Integer>();
		nwStats = new ArrayList<Integer>();
		wbStats = new ArrayList<Integer>();
		forwardStats = new ArrayList<Integer>();
		wrongPredT = 0;
		wrongPredNT = 0;
		rightPredT = 0;
		rightPredNT = 0;

                ////// PARSE INPUT INTO MEMORY AND INSTRUCTION CACHE HERE ////////
                Parser parser = new Parser();
                try {
                        parser.read(new FileInputStream(benchmark));
                        instCache = parser.returnInstCache();
                        memoCache = parser.returnMemoryCache();
                }
                catch (FileNotFoundException e) {
                        System.out.println("Benchmark file ("+benchmark+") was not found");
                        e.printStackTrace();
                        System.exit(1);
                }

                HashMap<String, Integer> stringLoc = parser.returnBranchLoc();               


                // MAIN PROGRAM - loop until PC is past last instr address
                int invalidPC = instCache.size(); // THIS WON'T WORK - STILL NEED TO WB
				int cacheLineStart = 0;
				int cacheLineEnd = 8;
				int iterations = 0;
				globalCycles = 1;
                while (iterations<DESIRED_RUNNING_CYCLES) {
		                System.out.println("***************cycle num: "+globalCycles);
		                
		                //////////// FIRST SECTION - REORDER BUFFR ////////////
		                // write back however many are read to be written back as long as they are in order
		                // write back up to 4 (specified by the project)
		                int writtenBack = 0;
		                for (int i = 0; i < 4; i++) {
							//test if you can write back
							if (rob.canCommit()) {
								//System.out.println("\n\nCAN WRITE BACK\n\n");
								// writes back single instr in ROB
								rob.commit();
								writtenBack++;
							}
		
							// if no, stall, because they have to commit in order
							else { i = 4; }
		                }
		                wbStats.add(writtenBack);
                        
                        /////////// SECOND SECTION - ISSUE ///////////////////
                        // call execute ( do stuff in all functional units
                        //   update counts of cycles for all functional units that are more than 1 cycel
                        //   for up to 4 instructions
                        //     if complete, set some kind of flag in reorder buffer for WB
                        // (branch target stuff will probably happen in this stage)
                        //
                        //
                        // for up to 4 instructions
                        //   check if reservation station for FU is available
                        //   check if there is a space in reorder buffer
                        //   if store, check if mem_store_q has space
                        //   if the previous are true:
                        //     read values of appropriate regs (src)
                        //     rename happens here
                        //       dest reg has to be associated with a reorder buffer
                        
                        // 1st step - execute
                        // check if there's instruction in the function unit
                        // execute based on the remaining cycle number and availability of ROB
                        Instruction exeInst;
                        int intResult;
                        double fpResult;
                        // add cycles if there's cache miss in LS
                        int countLSAccess = 0;
                        
                        if(xsu0.hasInst()){
	                		exeInst = xsu0.get(0);
	                		// check if there's ROB
	                		// always retrieve value from the first item
	                		if(!exeInst.getrSrc1().contains("ROB")) {
	                			exeInst.setFuCount(exeInst.getFuCount()-1);
	                		}
	                		if(exeInst.getFuCount()==0){
	                			intResult = execUnit.executeXSU(exeInst);
	                			xsu0.remove(exeInst);
	                			// can be written back
	                			// assign execution value to the ROB index
	                			rob.setwbFlagTrue(exeInst.getrDest());
	                			rob.setIntVal(exeInst.getrDest(), intResult);
	                			//System.out.println("result: "+intResult);
	                		}
                        }
                        if(xsu1.hasInst()){
	                		exeInst = xsu1.get(0);
	                		if(!exeInst.getrSrc1().contains("ROB")) {
	                			exeInst.setFuCount(exeInst.getFuCount()-1);
	                		}
	                		if(exeInst.getFuCount()==0){
	                			intResult = execUnit.executeXSU(exeInst);
	                			xsu1.remove(exeInst);
	                			rob.setwbFlagTrue(exeInst.getrDest());
	                			rob.setIntVal(exeInst.getrDest(), intResult);
	                			//System.out.println("result: "+intResult);
	                		}
                        }
                        if(ls.hasInst()){
                    		exeInst = ls.get(0);
            				System.out.println("-----*****"+exeInst.getFuCount());
                    		if(!exeInst.getrSrc1().contains("ROB")) {
	                			exeInst.setFuCount(exeInst.getFuCount()-1);
	                			if((((countLSAccess+1)%5)==0)&&((exeInst.getOperation().equals("L.D"))||
	                					(exeInst.getOperation().equals("LD")))){
	                				exeInst.setFuCount(exeInst.getFuCount()+4);
	                			} else if((((countLSAccess+1)%5)==0)&&((exeInst.getOperation().equals("S.D"))||
	                					(exeInst.getOperation().equals("SD")))){
	                				exeInst.setFuCount(exeInst.getFuCount()+5);
	                			}
	                		}
                    		if(exeInst.getFuCount()==0){
                    			if((exeInst.getOperation().equals("L.D"))){                        				
                    				fpResult = execUnit.executeFPLS(exeInst,memoCache);
                    				ls.remove(exeInst);
                    				rob.setwbFlagTrue(exeInst.getrDest());
                    				rob.setFPVal(exeInst.getrDest(), fpResult);
                    				//System.out.println("result: "+fpResult);
                    			} else if((exeInst.getOperation().equals("LD"))){
                    				intResult = execUnit.executeINTLS(exeInst, memoCache);
                    				ls.remove(exeInst);
                    				rob.setwbFlagTrue(exeInst.getrDest());
                    				rob.setIntVal(exeInst.getrDest(), intResult);
                    				//System.out.println("result: "+intResult);
                    			} else if(exeInst.getOperation().equals("S.D")){
                    				StoreBufferEntry sbEntry = new StoreBufferEntry();
                    				intResult = (int)execUnit.executeFPLS(exeInst,memoCache);
                    				ls.remove(exeInst);
                    				// set value to the store buffer entry
                    				sbEntry.setFpFlag(true);
                    				sbEntry.setFpval(Double.parseDouble(exeInst.getrSrc1()));
                    				sbEntry.setMemAddress(intResult);
                    				linkToSDEntry.put(exeInst.getrDest(), sbEntry);
                    				rob.setwbFlagTrue(exeInst.getrDest());
                    			} else if(exeInst.getOperation().equals("SD")){
                    				StoreBufferEntry sbEntry = new StoreBufferEntry();
                    				intResult = execUnit.executeINTLS(exeInst, memoCache);
                    				ls.remove(exeInst);
                    				// set value to the store buffer entry
                    				sbEntry.setFpFlag(false);
                    				sbEntry.setIntval(Integer.parseInt(exeInst.getrSrc1()));
                    				sbEntry.setMemAddress(intResult);
                    				linkToSDEntry.put(exeInst.getrDest(), sbEntry);
                    				rob.setwbFlagTrue(exeInst.getrDest());
                    			}
                    		}
                        }
                        if(fpu.hasInst()){
                    		exeInst = fpu.get(0);
                    		if((!exeInst.getrSrc1().contains("ROB"))&&(!exeInst.getrSrc2().contains("ROB"))) {
	                			exeInst.setFuCount(exeInst.getFuCount()-1);
	                		}
                    		if(exeInst.getFuCount()==0){
                    			fpResult = execUnit.executeFPU(exeInst);
                    			fpu.remove(exeInst);
                    			rob.setwbFlagTrue(exeInst.getrDest());
                    			rob.setFPVal(exeInst.getrDest(), fpResult);
                    			//System.out.println("result: "+intResult);
                    		}
                        }
                        if(mcfxu.hasInst()){
	                		exeInst = mcfxu.get(0);
	                		if((!exeInst.getrSrc1().contains("ROB"))&&(!exeInst.getrSrc2().contains("ROB"))) {
	                			exeInst.setFuCount(exeInst.getFuCount()-1);
	                		}
	                		if(exeInst.getFuCount()==0){
	                			intResult = execUnit.executeMCF(exeInst);
	                			mcfxu.remove(exeInst);
	                			rob.setwbFlagTrue(exeInst.getrDest());
	                			rob.setIntVal(exeInst.getrDest(), intResult);
	                			//System.out.println("result: "+intResult);
	                		}
                        }
                        if(bpu.hasInst()){
                       		exeInst = bpu.get(0);
		                	if((!exeInst.getrSrc1().contains("ROB"))) {
		                		exeInst.setFuCount(exeInst.getFuCount()-1);
		                	}
                    		if(exeInst.getFuCount()==0){
                    			if(execUnit.executeBPU(exeInst)){ //BRANCH TAKEN
						if (exeInst.getrSrc2() != null) {
							if (branchPred) { // using branch prediction
								if (branchTargetBuffer[(exeInst.getAddress())%(branchTargetBuffer.length)] == -1) {
									int ad = stringLoc.get(exeInst.getrSrc2());	
									PC = ad;
									updatedPC = true;
									// branch is taken, need to flush the instruction after the branch
									flush_list = rob.clear(exeInst.getrDest());
									//System.out.println("-----*****"+flush_list.toString());

									// add to branch target buffer
									branchTargetBuffer[(exeInst.getAddress())%(branchTargetBuffer.length)] = ad;
								}
								else {
									int ad = stringLoc.get(exeInst.getrSrc2());	
									if (ad != branchTargetBuffer[(exeInst.getAddress())%(branchTargetBuffer.length)]) { // predicted not taken and wrong
										PC = ad;
										updatedPC = true;
										flush_list = rob.clear(exeInst.getrDest());
										branchTargetBuffer[(exeInst.getAddress())%(branchTargetBuffer.length)] = ad;
										wrongPredNT++;
									}
									else { rightPredT++; }
								}
							}
							else {// no branch prediction
								int ad = stringLoc.get(exeInst.getrSrc2());	
								PC = ad;
								updatedPC = true;
								// branch is taken, need to flush the instruction after the branch
								flush_list = rob.clear(exeInst.getrDest());
								//System.out.println("-----*****"+flush_list.toString());
					
							}
			                   	 }
                    			}
					else { // BRANCH NOT TAKEN 
						if (exeInst.getrSrc2() != null) {
							if (branchPred) { 
								// add to branch target buffer
								if (branchTargetBuffer[(exeInst.getAddress())%(branchTargetBuffer.length)]==-1) {
									branchTargetBuffer[(exeInst.getAddress())%(branchTargetBuffer.length)] = exeInst.getAddress()+1;
								}
								else if (exeInst.getAddress()+1 != branchTargetBuffer[(exeInst.getAddress())%(branchTargetBuffer.length)]) {	// predicted taken and wrong
									int ad = stringLoc.get(exeInst.getrSrc2());	
									PC = exeInst.getAddress()+1;
									updatedPC = true;
									// branch is taken, need to flush the instruction after the branch
									flush_list = rob.clear(exeInst.getrDest());
									branchTargetBuffer[(exeInst.getAddress())%(branchTargetBuffer.length)] = exeInst.getAddress()+1;
									wrongPredT++;
								}
								else { rightPredNT++;}
							}
						}
						// no penalty for branch not taken and no branch prediction (default)
					}
                    			bpu.remove(exeInst);
                    			rob.setwbFlagTrue(exeInst.getrDest());
                    		}
                        }
                        
                        // 2nd step - issue/dispatch
                        int numIssued = 0;
                        for (int i = 0; i < NW; i++) {
                        	// get operand types in order to get specific FU
                        	// if specific FU hasSpace() && reorder_buffer.hasSpace()
                        	//	change instruction for rename
                        	//	insert this changed inst into FU and ROB
                        	Instruction inst = issue_queue.peek();
                        	if(inst != null) {
                        		String funcUnit = inst.getFuncUnit();
                        		Boolean rob_avail = rob.hasSpace();
                        		Instruction returnInst;
                        		// distribute to two XSUs
                        		int flag0 = xsu0.spaceAvailable();
                        		int flag1 = xsu1.spaceAvailable();
                        		// insert each instruction into corresponding function unit
                        		// add instruction to reorder buffer at the same time
                        		// update the destination address of each instruction to ROB index
                        		// check if either rSrc1 or rSrc2 appears in ROB
                        		// if true, replace it with ROB index
                        		if(funcUnit.equals("XSU")){
                        			if(rob_avail&&(flag0>=flag1)&&(xsu0.hasSpace())){
                        				// return ROB index
                        				String currentROB = rob.add(inst);
                        				// set destination to ROB index
                        				inst.setrDest(currentROB);
                        				// get returned instruction with substituted source register
                        				returnInst = rob.replaceAllRegs(currentROB);
                        				xsu0.add(returnInst);
                        				flag0--;
                        				issue_queue.remove();
                        				numIssued++;
                        			} else if(rob_avail&&(flag0<flag1)&&(xsu1.hasSpace())){
                        				String currentROB = rob.add(inst);
                        				inst.setrDest(currentROB);
                        				returnInst = rob.replaceAllRegs(currentROB);
                        				xsu1.add(returnInst);
                        				flag1--;
                        				issue_queue.remove();
                        				numIssued++;
                        			} else {i = NW;}
                        			// flush the instruction if the branch is taken
                        			if(flush_list.size()!=0){
                        				for(int j=0; j<flush_list.size(); j++){
                        					for(int k=0; k<xsu0.size(); k++){
                        						if(flush_list.get(j).equals(xsu0.get(k).getrDest())){
                        							xsu0.remove(k);
                        						}
                        					}
                        					for(int k=0; k<xsu1.size(); k++){
                        						if(flush_list.get(j).equals(xsu1.get(k).getrDest())){
                        							xsu1.remove(k);
                        						}
                        					}
                        				}
                        			}
                        		} else if (funcUnit.equals("LS")){
                        			if(ls.hasSpace()&&rob_avail){
                        				String currentROB = rob.add(inst);
                        				inst.setrDest(currentROB);
                        				returnInst = rob.replaceAllRegs(currentROB);
                        				ls.add(returnInst);
                        				issue_queue.remove();
                        				numIssued++;
                        			} else {i = NW;}
                        			// flush the instruction if the branch is taken
                        			if(flush_list.size()!=0){
                        				for(int j=0; j<flush_list.size(); j++){
                        					for(int k=0; k<ls.size(); k++){
                        						if(flush_list.get(j).equals(ls.get(k).getrDest())){
                        							ls.remove(k);
                        						}
                        					}
                        				}
                        			}
                        		} else if (funcUnit.equals("FPU")){
                        			if(fpu.hasSpace()&&rob_avail){
                        				String currentROB = rob.add(inst);
                        				inst.setrDest(currentROB);
                        				returnInst = rob.replaceAllRegs(currentROB);
                        				fpu.add(inst);
                        				issue_queue.remove();
                        				numIssued++;
                        			} else {i = NW;}
                        			// flush the instruction if the branch is taken
                        			if(flush_list.size()!=0){
                        				for(int j=0; j<flush_list.size(); j++){
                        					for(int k=0; k<fpu.size(); k++){
                        						if(flush_list.get(j).equals(fpu.get(k).getrDest())){
                        							fpu.remove(k);
                        						}
                        					}
                        				}
                        			}
                        		} else if (funcUnit.equals("MCF")){
                        			if(mcfxu.hasSpace()&&rob_avail){
                        				String currentROB = rob.add(inst);
                        				inst.setrDest(currentROB);
                        				returnInst = rob.replaceAllRegs(currentROB);
                        				mcfxu.add(returnInst);
                        				issue_queue.remove();
                        				numIssued++;
                        			} else {i = NW;}
                        			// flush the instruction if the branch is taken
                        			if(flush_list.size()!=0){
                        				for(int j=0; j<flush_list.size(); j++){
                        					for(int k=0; k<mcfxu.size(); k++){
                        						if(flush_list.get(j).equals(mcfxu.get(k).getrDest())){
                        							mcfxu.remove(k);
                        						}
                        					}
                        				}
                        			}
                        		} else if (funcUnit.equals("BPU")){
                        			if(bpu.hasSpace()&&rob_avail){
                        				String currentROB = rob.add(inst);
                        				inst.setrDest(currentROB);
                        				returnInst = rob.replaceAllRegs(currentROB);
                        				bpu.add(returnInst);
                        				issue_queue.remove();
                        				numIssued++;
                        			} else {i = NW;}
                        		}
                        	}
                        	else {
                        		i = NW;
                        	}
                        }
                        // statistics for num issued
                        nwStats.add(numIssued);

                        // print out items in ROB
                        System.out.println("\n###### reorder_buffer contents ######");
                        rob.printAllContents();
                        System.out.println();
                        
                        // print out instructions in each function unit
                        System.out.println("###### function_units contents ######");
                        System.out.println("XSU0: ");
                        for(int i=0; i<xsu0.size(); i++){
                        	System.out.println(xsu0.get(i).getContent());
                        }
                        System.out.println("XSU1: ");
                        for(int i=0; i<xsu1.size(); i++){
                        	System.out.println(xsu1.get(i).getContent());
                        }
                        System.out.println("Load/Store: ");
                        for(int i=0; i<ls.size(); i++){
                        	System.out.println(ls.get(i).getContent());
                        }
                        System.out.println("FPU: ");
                        for(int i=0; i<fpu.size(); i++){
                        	System.out.println(fpu.get(i).getContent());
                        }
                        System.out.println("MCFXU: ");
                        for(int i=0; i<mcfxu.size(); i++){
                        	System.out.println(mcfxu.get(i).getContent());
                        }
                        System.out.println("BPU: ");
                        for(int i=0; i<bpu.size(); i++){
                        	System.out.println(bpu.get(i).getContent());
                        }
                        System.out.println();
                        

			// IF PC WAS UPDATED IN BRANCH (this means branch taken - predicted not taken)
			if (updatedPC) {
				// clear issue queue
				while (issue_queue.size() != 0) {
					issue_queue.poll();
				}
				// clear instruction queue
				while (inst_queue.size() != 0) {
					inst_queue.poll();
				}
				//update cache line
				if (PC%8 == 0) {
					cacheLineStart = PC;
					cacheLineEnd = cacheLineStart + 8;
				}
				else {
					cacheLineStart = PC - (PC%8);
					cacheLineStart = PC;
					cacheLineEnd = cacheLineStart + 8;	
				}
				updatedPC = false;
			}


			// PRINT REGISTERS after updated
			registers.printAll();
			System.out.println("******** Data Cache *****************");
			System.out.println(memoCache.toString());
                        /////////// THIRD SECTION - DECODE ///////////////////
                        // (we already did this, but pretend it happens here)
                        // check how much space is availble in issue queue (up to 4)
                        // "decode"
                        // put up to 4 instr in issue queue
                        //issue_queue
			
			for (int i = 0; i < ND; i++) {
				// remove from instruction queue
				Instruction n = inst_queue.peek();
				if (n != null) {
					// decode - already happened
					// add to issue queue
					boolean added = issue_queue.offer(n);
					if (!added) { i = ND; }
					else { inst_queue.poll();}
				}
				else { i = ND; }
			}
                        System.out.println("\n###### issue_queue contents ######");
            			for(Instruction i:issue_queue){
            				System.out.println(i.getContent());
            			}
                        System.out.println();

                        
                        ////////// FOURTH SECTION - FETCH ////////////////////
                        // x instructions are available in I-Cache 
                        // if 4 instr are in cache line (8 words or 8 intsructions)
                        //     read 4 instr
                        // else
                        //     read remaining instr in cache line
                        // update cache line ptr if necessary
                        // put instr into instr queue
			//inst_queue
			
			// bring NF instr from current cache line to 
            Parser newParser = new Parser();
            try {
            		newParser.read(new FileInputStream(benchmark));
                    instCache = newParser.returnInstCache();
            }
            catch (FileNotFoundException e) {
                    System.out.println("Benchmark file ("+benchmark+") was not found");
                    e.printStackTrace();
                    System.exit(1);
            }
			for (int i = 0; i < NF; i++) {                        
				if (PC < invalidPC && PC < cacheLineEnd) {
					boolean added = inst_queue.offer(instCache.get(PC));
					PC++;
					if (!added) { i = NF; }
				}
				else { i = NF; }
                	}
			// update cache line
			if (PC == cacheLineEnd) {
				cacheLineStart = cacheLineEnd;
				cacheLineEnd = cacheLineStart + 8;
			}
                	System.out.println("###### inst_queue contents ######");
        			for(Instruction i:inst_queue){
        				System.out.println(i.getContent());
        			}
                        System.out.println("new PC: "+PC+" ***************");
			System.out.println("\n\n");

			// GET REORDERBUFFER STATISTICS
			rbrsStats.add(rob.getNum());
			intrbStats.add(rob.getNumInt());
			fprbStats.add(rob.getNumFP());

			// if all components empty, end simulation
			if ((inst_queue.size() == 0) && (issue_queue.size() == 0) && (rob.getNum() == 0) && (PC >= invalidPC)) {
				iterations = DESIRED_RUNNING_CYCLES;
			}

			// increment cycles
                        globalCycles++;
			iterations++;
                }
		


		//// Statistics
		int maxRB = getMax(rbrsStats);//stores overall num of reorder buffers/reservation stations used
		int avgRB = getAvg(rbrsStats);
		// renames
		int maxIntRB = getMax(intrbStats);// stores number of ROBs used for int
		int avgIntRB = getAvg(intrbStats);
		int maxFPRB = getMax(fprbStats);// stores number of ROBs used for fp
		int avgFPRB = getAvg(fprbStats);
		// CDBs
		int maxNW = getMax(nwStats);// stores count of num issued per cycle
		int avgNW = getAvg(nwStats);
		int maxWB = getMax(wbStats);// counts number written back per cycle
		int avgWB = getAvg(wbStats);
		//int maxfwd = getMax(forwardStats);// counts number of instr with value used in reservation stations
		//int avgfwd = getAvg(forwardStats);
	
                
                ////////////// PRINT OUT STATISTICS HERE ///////////////////
                System.out.println("******************* Statistics *******************");
		System.out.println("The following statistics show the average number of a particular part used for a cycle and the maximum for a cycle.\n");
		System.out.println("NF = "+NF+", NQ = "+NQ+", ND = "+ND+", NI = "+NI+", NW = "+NW+", NR = "+NR+"\n");
                System.out.println("Number of cycles: " + globalCycles+"\n");
                System.out.println("Utilization of reservation stations, out of 21");
                System.out.println("Maximum: "+maxRB+"\nAverage: "+avgRB+"\n");
                System.out.println("Utilization of reorder buffers, out of "+NR);
                System.out.println("Maximum: "+maxRB+"\nAverage: "+avgRB+"\n");
                System.out.println("Utilization of rename registers");
                System.out.println("Maximum FP: "+maxFPRB+"\nAverage FP:"+avgFPRB);
                System.out.println("Maximum Int: "+maxIntRB+"\nAverage Int: "+avgIntRB+"\n");
                System.out.println("Issues, out of "+NW);
                System.out.println("Maximum: "+maxNW+"\nAverage: "+avgNW+"\n");
		System.out.println("CDBs, out of 4");
		System.out.println("Number written during a commit:");
                System.out.println("Maximum: "+maxNW+"\nAverage: "+avgNW+"\n");
		if (branchPred) {
			System.out.println("Number of branch predictions: ");
			System.out.println("Predicted taken when branch was not taken: "+wrongPredT);
			System.out.println("Predicted not when branch was taken: "+wrongPredNT);
			System.out.println("Predicted taken when branch was taken: "+rightPredT);
			System.out.println("Predicted not taken when branch was not taken: "+rightPredNT);
		}
		//System.out.println("Number used when forwarded to reservation stations during a commit:");
                //System.out.println("Maximum: "+maxfwd+"\nAverage: "+avgfwd+"\n");
                
        }

	// statistics-gathering functions
	public static int getMax(ArrayList<Integer> arr) {
		if (arr == null) return 0;
		int curHigh = 0;
		for (int i = 0; i < arr.size(); i++) {
			if (arr.get(i) > curHigh) curHigh = arr.get(i);
		}
		return curHigh;
	}
	public static int getAvg(ArrayList<Integer> arr) {
		if (arr == null) return 0;
		int sum = 0;
		for (int i = 0; i < arr.size(); i++) {
			sum = sum + arr.get(i);
		}
		if (arr.size() == 0) return 0;
		return (sum/arr.size());
	}
}

